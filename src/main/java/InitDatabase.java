import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InitDatabase {//https://vertex-academy.com/tutorials/ru/kak-podklyuchitsya-k-baze-dannyx-postgresql-s-pom/
    //  Database credentials
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/database1";
    private static final String USER = "username";
    private static final String PASS = "password";
    private static  Connection connection=null;

    public InitDatabase() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            connection.setAutoCommit(true);
            Statement statement = connection.createStatement();
            statement.executeUpdate("DROP DATABASE IF EXISTS database1");
            statement.execute("CREATE DATABASE database1");
            connection.setAutoCommit(false);
            statement.executeUpdate("CREATE TABLE NODES (ID BIGINT PRIMARY KEY NOT NULL, LAT DOUBLE PRECISION, LON DOUBLE PRECISION, USER_NAME TEXT, UID BIGINT, VISIBLE BOOLEAN, VERSION BIGINT, CHANGESET BIGINT,  _TIMESTAMP TIMESTAMPTZ)");
            statement.close();
            connection.commit();
        } catch (SQLException  |  ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }
    }
    public void disconnect(){
        try {
            connection.close();
            connection = null;
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    public Connection getConnection(){
        return connection;
    }


}
