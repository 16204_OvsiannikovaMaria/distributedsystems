import org.openstreetmap.osm._0.Node;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;

class NodeDAO {
    private static Connection connection=null;
    private static int amount = 0;
    private static long timeInsert = 0, timePreparedStatement = 0, timeBatch = 0;
    private static int sposob = 0;
    private static java.util.List<Node> listNodes = new ArrayList();
    void setConnection(Connection connection){
        NodeDAO.connection = connection;
    }
    void addToDatabase(Node node) throws SQLException{
        amount++;
        if (sposob==1)
            usingInsert(node);
        else if (sposob==2)
            usingPreparedStatement(node);
        else if (sposob==3)
            usingBatch(node);
    }
    void setSposob(int num){
        sposob = num;
        amount=0;
    }
    String getStatistics(){
        double timeInsert =   (double)amount / (double)(NodeDAO.timeInsert *1000);
        double timePreparedStatement =   (double)amount / (double)(NodeDAO.timePreparedStatement *1000);
        double timeBatch =   (double)amount / (double)(NodeDAO.timeBatch *1000);
        return "\tВремя вставок (Записей в секунду):\nСпособ 1 [С помощью конструирования INSERT как строки]: "+timeInsert+".\nСпособ 2 [С помощью PreparedStatement]: "+ timePreparedStatement+".\nСпособ 3 [С помощью Batch]: "+ timeBatch+".";
    }
    private void usingInsert(Node node) throws SQLException {
        long startTime, endTime;
        startTime = System.currentTimeMillis();
        connection.setAutoCommit(false);
        Statement statement = connection.createStatement();
        Timestamp timestamp = new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis());
        statement.executeUpdate( "INSERT INTO NODES (ID, LAT, LON, USER_NAME, UID, VISIBLE, VERSION, CHANGESET, _TIMESTAMP) VALUES (" + node.getId() + ", " + node.getLat().toString().replace(',', '.') + ", "+node.getLon().toString().replace(',', '.')+ "'" + node.getUser().replace('\'', '.') + "'" + ", " +node.getUid()+", "+node.isVisible()+", "+node.getVersion()+", "+node.getChangeset()+", "+"'" + timestamp + "');");
        connection.commit();
        statement.close();
        endTime = System.currentTimeMillis();
        timeInsert+=(endTime-startTime);
    }
    private void usingPreparedStatement(Node node) throws SQLException{
        long startTime, endTime;
        startTime = System.currentTimeMillis();
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO NODES (ID, LAT, LON, USER_NAME, UID, VISIBLE, VERSION, CHANGESET, _TIMESTAMP) VALUES (?,?,?,?,?,?,?,?,?)");
        preparedStatement.setBigDecimal(1, new BigDecimal(node.getId()));
        preparedStatement.setDouble(2, node.getLat());
        preparedStatement.setDouble(3, node.getLon());
        preparedStatement.setString(4, node.getUser());
        preparedStatement.setBigDecimal(5, new BigDecimal(node.getUid()));
        preparedStatement.setBoolean(6, node.isVisible());
        preparedStatement.setBigDecimal(7, new BigDecimal(node.getVersion()));
        preparedStatement.setBigDecimal(8, new BigDecimal(node.getChangeset()));
        preparedStatement.setTimestamp(9, new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis()));
        preparedStatement.executeUpdate();
        connection.commit();
        preparedStatement.close();
        endTime = System.currentTimeMillis();
        timePreparedStatement+=(endTime-startTime);
    }
    private void usingBatch(Node newNode) throws SQLException {
        if (listNodes.size()<19)
            listNodes.add(newNode);
        else {
            listNodes.add(newNode);
            long startTime, endTime;
            startTime = System.currentTimeMillis();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO NODES (ID, LAT, LON, USER_NAME, UID, VISIBLE, VERSION, CHANGESET, _TIMESTAMP) VALUES (?,?,?,?,?,?,?,?,?)");
            for (Node node : listNodes) {
                preparedStatement.setBigDecimal(1, new BigDecimal(node.getId()));
                preparedStatement.setDouble(2, node.getLat());
                preparedStatement.setDouble(3, node.getLon());
                preparedStatement.setString(4, node.getUser());
                preparedStatement.setBigDecimal(5, new BigDecimal(node.getUid()));
                preparedStatement.setBoolean(6, node.isVisible());
                preparedStatement.setBigDecimal(7, new BigDecimal(node.getVersion()));
                preparedStatement.setBigDecimal(8, new BigDecimal(node.getChangeset()));
                preparedStatement.setTimestamp(9, new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis()));
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            connection.commit();
            preparedStatement.close();
            endTime = System.currentTimeMillis();
            timeBatch+=(endTime-startTime);
            listNodes = new ArrayList<>();
        }
    }
    void addRestBatch() throws SQLException{
        if (listNodes.size()>0)
        {
            long startTime, endTime;
            startTime = System.currentTimeMillis();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO NODES (ID, LAT, LON, USER_NAME, UID, VISIBLE, VERSION, CHANGESET, _TIMESTAMP) VALUES (?,?,?,?,?,?,?,?,?)");
            for (Node node : listNodes) {
                preparedStatement.setBigDecimal(1, new BigDecimal(node.getId()));
                preparedStatement.setDouble(2, node.getLat());
                preparedStatement.setDouble(3, node.getLon());
                preparedStatement.setString(4, node.getUser());
                preparedStatement.setBigDecimal(5, new BigDecimal(node.getUid()));
                preparedStatement.setBoolean(6, node.isVisible());
                preparedStatement.setBigDecimal(7, new BigDecimal(node.getVersion()));
                preparedStatement.setBigDecimal(8, new BigDecimal(node.getChangeset()));
                preparedStatement.setTimestamp(9, new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis()));
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            connection.commit();
            preparedStatement.close();
            endTime = System.currentTimeMillis();
            timeBatch+=(endTime-startTime);
            listNodes = new ArrayList<>();
        }
    }

}
