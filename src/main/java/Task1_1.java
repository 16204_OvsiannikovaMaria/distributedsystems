import org.openstreetmap.osm._0.Node;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;

public class Task1_1 {
    public static void main(String[] args) {
        System.out.println("Hello!");
        try {
            unzipFile();
            for (int sposob = 1; sposob <= 3; sposob++) {//https://www.ibm.com/developerworks/ru/library/x-stax2/index.html
                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLEventReader reader = factory.createXMLEventReader(new FileInputStream(new File("RU-NVS.osm")));
                JAXBContext context = JAXBContext.newInstance(Node.class);
                Unmarshaller unmarshalNode = context.createUnmarshaller();
                InitDatabase database = new InitDatabase();
                Connection connection = database.getConnection();
                NodeDAO nodeDao = new NodeDAO();
                nodeDao.setConnection(connection);
                nodeDao.setSposob(sposob);
                while (reader.hasNext()) {
                    XMLEvent event = reader.nextEvent();//https://docs.oracle.com/javase/tutorial/jaxp/stax/api.html
                    if ((event.isStartElement()) && (event.asStartElement().getName().toString().equals("node"))) {  //https://docs.oracle.com/javase/tutorial/jaxp/stax/using.html атрибуты
                        Node node = (org.openstreetmap.osm._0.Node) unmarshalNode.unmarshal(reader);
                        nodeDao.addToDatabase(node);
                    }
                }
                if (sposob==3) {
                    nodeDao.addRestBatch();
                    System.out.println(nodeDao.getStatistics());
                }
                database.disconnect();
                reader.close();
            }
        } catch (XMLStreamException | JAXBException | SQLException | IOException e) {
            e.printStackTrace();
        }
    }
    private static void unzipFile() throws FileNotFoundException, IOException {//http://commons.apache.org/proper/commons-compress/examples.html
        BZip2CompressorInputStream bzIn = new BZip2CompressorInputStream(new FileInputStream(new File("RU-NVS.osm.bz2")));
        FileOutputStream out = new FileOutputStream(new File("RU-NVS.osm"));
        int buffersize = 2048;
        final byte[] buffer = new byte[buffersize];
        int n = 0;
        while (-1 != (n = bzIn.read(buffer))) {
            out.write(buffer, 0, n);
        }
        out.close();
        bzIn.close();
    }
}
